import React from "react";
import {
    View,
    RefreshControl,
    StyleSheet,
    TouchableOpacity
} from "react-native";
import moment from "moment-timezone";
import {
    Container,
    Content,
    Card,
    CardItem,
    Thumbnail,
    Text,
    Body,
    List,
    Tabs,
    Tab,
    Accordion,
    Spinner
} from "native-base";
import gql from "graphql-tag";
import {Query} from "react-apollo";

import {StackActions} from "react-navigation";

const FEED_QUERY = gql`
  query {
    categories {
      name
      id
      tournaments (where: {
      year: 2019,
      season: 2
      }){
        id
        year
        season
        events {
          id
          date
          matches {
            time
            home_team {
              name
              logo
            }
            away_team {
              name
              logo
            }
            highlights {
              player {
                name
              }
              team {
                name
              }
              action {
                description
              }
            }
          }
        }
      }
    }
  }
`;

export default class Fixture extends React.Component {
    _renderContent = item => item.content;

    tableGenerator = category =>
        category.tournaments[0].events.reduce((prev, current, index, events) => {
            let resumenFecha = events[index].matches.reduce(
                (prev, current, index, matches) => {
                    const homeTeam = matches[index].home_team.name;
                    const awayTeam = matches[index].away_team.name;
                    const homeLogo = matches[index].home_team.logo;
                    const awayLogo = matches[index].away_team.logo;
                    const time = matches[index].time;
                    const {
                        homeGoals,
                        awayGoals,
                        homeYellows,
                        homeReds,
                        awayYellows,
                        awayReds
                    } = matches[index].highlights.reduce(
                        (prev, current, index, highlights) => {
                            let temp = {...prev};

                            if (
                                highlights[index].action.description === "G" &&
                                highlights[index].team.name === homeTeam
                            ) {
                                temp.homeGoals = [
                                    ...temp.homeGoals,
                                    {
                                        playerName: highlights[index].player.name
                                    }
                                ];
                            }
                            if (
                                highlights[index].action.description === "G" &&
                                highlights[index].team.name === awayTeam
                            ) {
                                temp.awayGoals = [
                                    ...temp.awayGoals,
                                    {
                                        playerName: highlights[index].player.name
                                    }
                                ];
                            }
                            if (
                                highlights[index].action.description === "TA" &&
                                highlights[index].team.name === homeTeam
                            ) {
                                temp.homeYellows = [
                                    ...temp.homeYellows,
                                    {
                                        playerName: highlights[index].player.name
                                    }
                                ];
                            }
                            if (
                                highlights[index].action.description === "TA" &&
                                highlights[index].team.name === awayTeam
                            ) {
                                temp.awayYellows = [
                                    ...temp.awayYellows,
                                    {
                                        playerName: highlights[index].player.name
                                    }
                                ];
                            }

                            if (
                                highlights[index].action.description === "TR" &&
                                highlights[index].team.name === homeTeam
                            ) {
                                temp.homeReds = [
                                    ...temp.homeReds,
                                    {
                                        playerName: highlights[index].player.name
                                    }
                                ];
                            }
                            if (
                                highlights[index].action.description === "TR" &&
                                highlights[index].team.name === awayTeam
                            ) {
                                temp.awayReds = [
                                    ...temp.awayReds,
                                    {
                                        playerName: highlights[index].player.name
                                    }
                                ];
                            }

                            return temp;
                        },
                        {
                            homeGoals: [],
                            awayGoals: [],
                            homeYellows: [],
                            homeReds: [],
                            awayYellows: [],
                            awayReds: []
                        }
                    );
                    return [
                        ...prev,
                        {
                            time,
                            homeLogo,
                            awayLogo,
                            homeTeam,
                            awayTeam,
                            homeGoals,
                            awayGoals,
                            homeYellows,
                            homeReds,
                            awayYellows,
                            awayReds
                        }
                    ];
                },
                []
            );
            return [...prev, {date: events[index].date, matches: resumenFecha}];
        }, []);

    listContent = fechas => {
        return fechas.map((item, key) => ({
            title: `Fecha ${key + 1}`,
            content: (
                <View key={key} style={{flexDirection: "column"}}>
                    {item.matches.map((match, key) => (
                        <TouchableOpacity
                            key={key}
                            onPress={() => this.handleMatchTap(match)}
                        >
                            <Card style={{width: "100%"}}>
                                <CardItem header>
                                    <Body style={styles.cardFirstItem}>
                                        {match.homeLogo && (
                                            <Thumbnail
                                                source={{
                                                    uri: match.homeLogo
                                                }}
                                            />
                                        )}
                                        <Text style={{fontSize: 12}}>{match.homeTeam}</Text>
                                        <Text style={{fontSize: 12}}>Vs</Text>
                                        <Text style={{fontSize: 12}}>{match.awayTeam}</Text>

                                        {match.awayLogo && (
                                            <Thumbnail source={{uri: match.awayLogo}}/>
                                        )}
                                    </Body>
                                </CardItem>
                                <CardItem>
                                    <Body style={styles.cardSecondItem}>
                                        <Text style={{fontWeight: "bold"}}>
                                            {match.homeGoals.length}
                                        </Text>
                                        <Text>{`${moment(match.time).format("HH:mm")} hs.`}</Text>
                                        <Text style={{fontWeight: "bold"}}>
                                            {match.awayGoals.length}
                                        </Text>
                                    </Body>
                                </CardItem>
                            </Card>
                        </TouchableOpacity>
                    ))}
                </View>
            )
        }));
    };

    handleMatchTap = detalles => {
        const pushAction = StackActions.push({
            routeName: "DetallesPartido",
            params: {
                detalles
            }
        });

        this.props.navigation.dispatch(pushAction);
    };

    render() {
        return (
            <Container>
                <Query query={FEED_QUERY}>
                    {({loading, error, data, refetch, networkStatus}) => {
                        return (
                            <Content
                                contentContainerStyle={{
                                    flex: error ? 1 : null
                                }}
                                refreshControl={
                                    <RefreshControl
                                        refreshing={loading}
                                        onRefresh={refetch}
                                        tintColor="#3478f6"
                                    />
                                }
                            >
                                {loading ? null : error ?
                                    <View
                                        style={{
                                            flex: 1,
                                            justifyContent: "center",
                                            alignItems: "center"
                                        }}
                                    >
                                        <Text> Error! Deslizá hacia abajo para volver a intentar </Text>
                                    </View> : (
                                        <Tabs
                                            bounces={false}
                                            contentProps={{indicatorStyle: "white"}}
                                            initialPage={0}
                                        >
                                            {data.categories.map((category, key) => (
                                                <CategoryTab
                                                    renderContent={this._renderContent}
                                                    key={key}
                                                    meta={{
                                                        nombre: category.name,
                                                        anho: category.tournaments[0].year
                                                    }}
                                                    heading={category.name}
                                                    retrieving={loading}
                                                    listContent={this.listContent(
                                                        this.tableGenerator(category)
                                                    )}
                                                />
                                            ))}
                                        </Tabs>
                                    )}
                            </Content>
                        );
                    }}
                </Query>
            </Container>
        );
    }
}

const TabContent = props => <List>{props.children}</List>;

const CategoryTab = props => (
    <Tab>
        <TabContent>
            {!props.retrieving ? (
                <Accordion
                    scrollEnabled={false}
                    dataArray={props.listContent}
                    renderContent={props.renderContent}
                />
            ) : (
                <Spinner/>
            )}
        </TabContent>
    </Tab>
);

const styles = StyleSheet.create({
    cardFirstItem: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },
    cardSecondItem: {
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center"
    }
});
