import React, { Component } from "react";
import { RefreshControl, StyleSheet } from "react-native";
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Body,
  Text,
  View
} from "native-base";
import moment from "moment";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class DetallesPartido extends Component {
  render() {
    const { detalles } = this.props.navigation.state.params;
    return (
      <Container>
        <Content padder>
          <Card>
            <CardItem bordered>
              <Body style={styles.cardFirstItem}>
                <View
                  style={{
                    flex: 33,
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  {detalles.homeLogo && (
                    <View>
                      <Thumbnail source={{ uri: detalles.homeLogo }} />
                    </View>
                  )}
                  <Text>{detalles.homeTeam}</Text>
                </View>
                <View
                  style={{
                    flex: 33,
                    flexDirection: "column",
                    alignItems: "center",
                    justifyContent: "center"
                  }}
                >
                  <Text>{moment(detalles.time).format("DD/MM")}</Text>
                  <Text style={{ fontWeight: "bold", fontSize: 20 }}>
                    {moment() > moment(detalles.time) &&
                      `${detalles.homeGoals.length} - ${
                        detalles.awayGoals.length
                      }`}
                  </Text>
                  <Text>{`${moment(detalles.time).format("HH:mm")} hs.`}</Text>
                </View>
                <View
                  style={{
                    flex: 33,
                    flexDirection: "column",
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
                  {detalles.awayLogo && (
                    <Thumbnail source={{ uri: detalles.awayLogo }} />
                  )}
                  <Text>{detalles.awayTeam}</Text>
                </View>
              </Body>
            </CardItem>
            {(detalles.homeGoals.length > 0 ||
              detalles.awayGoals.length > 0) && (
              <CardItem>
                <Body style={styles.cardSecondItem}>
                  <View
                    style={{
                      flexDirection: "column",
                      alignItems: "flex-start",
                      flex: 33
                    }}
                  >
                    {detalles.homeGoals.map((gol, key) => (
                      <Text style={{ fontSize: 14 }} key={key}>
                        {gol.playerName}
                      </Text>
                    ))}
                  </View>
                  <View>
                    <Icon name="soccer" size={25} />
                  </View>
                  <View
                    style={{
                      flexDirection: "column",
                      alignItems: "flex-end",
                      flex: 33
                    }}
                  >
                    {detalles.awayGoals.map((gol, key) => (
                      <Text style={{ fontSize: 14 }} key={key}>
                        {gol.playerName}
                      </Text>
                    ))}
                  </View>
                </Body>
              </CardItem>
            )}
            {(detalles.homeYellows.length > 0 ||
              detalles.awayYellows.length > 0) && (
              <CardItem>
                <Body style={styles.cardSecondItem}>
                  <View
                    style={{
                      flexDirection: "column",
                      alignItems: "flex-start",
                      flex: 33
                    }}
                  >
                    {detalles.homeYellows.map((tarjeta, key) => (
                      <Text style={{ fontSize: 14 }} key={key}>
                        {tarjeta.playerName}
                      </Text>
                    ))}
                  </View>
                  <View>
                    <Icon color="#ffcc22" name="cards" size={25} />
                  </View>
                  <View
                    style={{
                      flexDirection: "column",
                      alignItems: "flex-end",
                      flex: 33
                    }}
                  >
                    {detalles.awayYellows.map((tarjeta, key) => (
                      <Text style={{ fontSize: 14 }} key={key}>
                        {tarjeta.playerName}
                      </Text>
                    ))}
                  </View>
                </Body>
              </CardItem>
            )}
            {(detalles.homeReds.length > 0 || detalles.awayReds.length > 0) && (
              <CardItem>
                <Body style={styles.cardSecondItem}>
                  <View
                    style={{
                      flexDirection: "column",
                      alignItems: "flex-start",
                      flex: 33
                    }}
                  >
                    {detalles.homeReds.map((tarjeta, key) => (
                      <Text style={{ fontSize: 14 }} key={key}>
                        {tarjeta.playerName}
                      </Text>
                    ))}
                  </View>
                  <View>
                    <Icon color="#ff3b30" name="cards" size={25} />
                  </View>
                  <View
                    style={{
                      flexDirection: "column",
                      alignItems: "flex-end",
                      flex: 33
                    }}
                  >
                    {detalles.awayReds.map((tarjeta, key) => (
                      <Text style={{ fontSize: 14 }} key={key}>
                        {tarjeta.playerName}
                      </Text>
                    ))}
                  </View>
                </Body>
              </CardItem>
            )}
          </Card>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  cardFirstItem: {
    flex: 100,
    flexDirection: "row",
    justifyContent: "space-around",
    alignItems: "center"
  },
  cardSecondItem: {
    flex: 100,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  }
});
