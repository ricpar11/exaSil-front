import React, {Component} from "react";
import {
    StyleSheet,
    TouchableOpacity,
    RefreshControl,
    View
} from "react-native";
import {
    Container,
    Content,
    List,
    ListItem,
    Text,
    Tab,
    Tabs,
    Spinner
} from "native-base";

import {Query} from "react-apollo";
import gql from "graphql-tag";

import {StackActions} from "react-navigation";

const FEED_QUERY = gql`
  query {
  categories{
      name
      id
      tournaments(where: {
      year: 2019,
      season: 2
      }) {
        id
        year
        season
        events {
          id
          matches {
            played
            home_team {
              name
              logo
            }
            away_team {
              name
              logo
            }
            highlights {
              player {
                name
              }
              team {
                name
              }
              action {
                description
              }
            }
          }
        }
      }
    }
  }
`;

export default class Tabla extends Component {
    listContent = tabla => {
        return Object.keys(tabla).map((item, key) => (
            <ListItem
                key={key}
                style={{
                    marginLeft: 0,
                    paddingLeft: 0,
                    backgroundColor: `${key % 2 == 0 ? "#F9FCFE" : "#FFFFFF"}`
                }}
            >
                <TouchableOpacity
                    style={{
                        flex: 1,
                        flexDirection: "row",
                        backgroundColor: `${key % 2 == 0 ? "#F9FCFE" : "#FFFFFF"}`
                    }}
                    onPress={() => this.handleTeamTap(tabla[item])}
                >
                    <Text style={{flex: 0.4, marginLeft: 10, fontSize: 14}}>
                        {`${key + 1}. ${tabla[item].teamName}`}
                    </Text>
                    <Text style={styles.listContentDefaultItem}>
                        {tabla[item].partidosJugados}
                    </Text>

                    <Text style={styles.listContentDefaultItem}>
                        {tabla[item].partidosGanados}
                    </Text>
                    <Text style={styles.listContentDefaultItem}>
                        {tabla[item].partidosEmpatados}
                    </Text>
                    <Text style={styles.listContentDefaultItem}>
                        {tabla[item].partidosPerdidos}
                    </Text>
                    <Text style={styles.listContentDefaultItem}>
                        {tabla[item].golesaFavor}
                    </Text>
                    <Text style={styles.listContentDefaultItem}>
                        {tabla[item].golesEnContra}
                    </Text>
                    <Text style={styles.listContentDefaultItem}>
                        {tabla[item].golesaFavor - tabla[item].golesEnContra}
                    </Text>
                    <Text style={{...styles.listContentDefaultItem, fontWeight: "bold" }}>
                        {tabla[item].puntos}
                    </Text>
                </TouchableOpacity>
            </ListItem>
        ));
    };

    handleTeamTap = detalles => {
        const pushAction = StackActions.push({
            routeName: "DetallesEquipo",
            params: {
                detalles
            }
        });

        this.props.navigation.dispatch(pushAction);
    };

    tableGenerator = category => {
        return category.tournaments[0].events
            .reduce((prev, current, index, events) => {
                let {resumenFecha} = events[index].matches.reduce(
                    (prev, current, matchIndex, matches) => {
                        const homeTeam = matches[matchIndex].home_team.name;
                        const awayTeam = matches[matchIndex].away_team.name;
                        const homeLogo = matches[matchIndex].home_team.logo;
                        const awayLogo = matches[matchIndex].away_team.logo;
                        const {
                            homeGoals,
                            awayGoals,
                            homeYellows,
                            homeReds,
                            awayYellows,
                            awayReds
                        } = matches[matchIndex].highlights.reduce(
                            (prev, current, index, highlights) => {
                                let temp = {...prev};
                                if (
                                    highlights[index].action.description === "G" &&
                                    highlights[index].team.name === homeTeam
                                ) {
                                    temp.homeGoals = [
                                        ...temp.homeGoals,
                                        {
                                            playerName: highlights[index].player.name
                                        }
                                    ];
                                }
                                if (
                                    highlights[index].action.description === "G" &&
                                    highlights[index].team.name === awayTeam
                                ) {
                                    temp.awayGoals = [
                                        ...temp.awayGoals,
                                        {
                                            playerName: highlights[index].player.name
                                        }
                                    ];
                                }
                                if (
                                    highlights[index].action.description === "TA" &&
                                    highlights[index].team.name === homeTeam
                                ) {
                                    temp.homeYellows = [
                                        ...temp.homeYellows,
                                        {
                                            playerName: highlights[index].player.name
                                        }
                                    ];
                                }
                                if (
                                    highlights[index].action.description === "TA" &&
                                    highlights[index].team.name === awayTeam
                                ) {
                                    temp.awayYellows = [
                                        ...temp.awayYellows,
                                        {
                                            playerName: highlights[index].player.name
                                        }
                                    ];
                                }

                                if (
                                    highlights[index].action.description === "TR" &&
                                    highlights[index].team.name === homeTeam
                                ) {
                                    temp.homeReds = [
                                        ...temp.homeReds,
                                        {
                                            playerName: highlights[index].player.name
                                        }
                                    ];
                                }
                                if (
                                    highlights[index].action.description === "TR" &&
                                    highlights[index].team.name === awayTeam
                                ) {
                                    temp.awayReds = [
                                        ...temp.awayReds,
                                        {
                                            playerName: highlights[index].player.name
                                        }
                                    ];
                                }

                                return temp;
                            },
                            {
                                homeGoals: [],
                                awayGoals: [],
                                homeYellows: [],
                                homeReds: [],
                                awayYellows: [],
                                awayReds: []
                            }
                        );

                        return {
                            resumenFecha: {
                                ...prev.resumenFecha,
                                [homeTeam]: {
                                    jugado: !!matches[matchIndex].played,
                                    logo: homeLogo,
                                    resultado: {
                                        favor: homeGoals,
                                        contra: awayGoals,
                                        amarillas: homeYellows,
                                        rojas: homeReds
                                    },
                                    tipoResultado:
                                        homeGoals > awayGoals
                                            ? "V"
                                            : homeGoals < awayGoals
                                            ? "D"
                                            : "E"
                                },
                                [awayTeam]: {
                                    jugado: !!matches[matchIndex].played,
                                    logo: awayLogo,
                                    resultado: {
                                        favor: awayGoals,
                                        contra: homeGoals,
                                        amarillas: awayYellows,
                                        rojas: awayReds
                                    },
                                    tipoResultado:
                                        homeGoals < awayGoals
                                            ? "V"
                                            : homeGoals > awayGoals
                                            ? "D"
                                            : "E"
                                }
                            }
                        };
                    },
                    {resumenFecha: {}}
                );

                const actual = Object.keys(resumenFecha).reduce(
                    (prev, current, index, equipos) => {
                        const equipoActual = equipos[index];
                        return [
                            ...prev,
                            {
                                teamName: equipoActual,
                                logo: resumenFecha[equipoActual].logo,
                                goles:
                                    prev[equipoActual] !== undefined
                                        ? [
                                            ...prev[equipoActual],
                                            ...resumenFecha[equipoActual].resultado.favor
                                        ]
                                        : resumenFecha[equipoActual].resultado.favor,
                                amarillas:
                                    prev[equipoActual] !== undefined
                                        ? [
                                            ...prev[equipoActual],
                                            ...resumenFecha[equipoActual].resultado.amarillas
                                        ]
                                        : resumenFecha[equipoActual].resultado.amarillas,
                                rojas:
                                    prev[equipoActual] !== undefined
                                        ? [
                                            ...prev[equipoActual],
                                            ...resumenFecha[equipoActual].resultado.rojas
                                        ]
                                        : resumenFecha[equipoActual].resultado.rojas,
                                puntos:
                                    prev[equipoActual] !== undefined
                                        ? prev[equipoActual].puntos +
                                        !!resumenFecha[equipoActual].jugado ? (resumenFecha[equipoActual].tipoResultado === "V"
                                        ? 3
                                        : resumenFecha[equipoActual].tipoResultado === "E"
                                            ? 1
                                            : 0) : 0
                                        : resumenFecha[equipoActual].tipoResultado === "V" && !!resumenFecha[equipoActual].jugado
                                        ? 3
                                        : resumenFecha[equipoActual].tipoResultado === "E" && !!resumenFecha[equipoActual].jugado
                                            ? 1
                                            : 0,
                                partidosJugados:
                                    prev[equipoActual] !== undefined
                                        ? prev.tabla[equipoActual].partidosJugados + (!!prev[equipoActual].jugado ? 1 : 0)
                                        : !!resumenFecha[equipoActual].jugado ? 1 : 0,
                                partidosGanados:
                                    prev[equipoActual] !== undefined &&
                                    resumenFecha[equipoActual].tipoResultado === "V"
                                        ? !!resumenFecha[equipoActual].jugado ? prev.tabla[equipoActual].partidosGanados + 1 : prev.tabla[equipoActual].partidosGanados
                                        : !!resumenFecha[equipoActual].jugado && resumenFecha[equipoActual].tipoResultado === "V"
                                        ? 1
                                        : 0,
                                partidosEmpatados:
                                    prev[equipoActual] !== undefined &&
                                    resumenFecha[equipoActual].tipoResultado === "E"
                                        ? !!resumenFecha[equipoActual].jugado ? prev.tabla[equipoActual].partidosEmpatados + 1 : prev.tabla[equipoActual].partidosEmpatados
                                        : !!resumenFecha[equipoActual].jugado && resumenFecha[equipoActual].tipoResultado === "E"
                                        ? 1
                                        : 0,
                                partidosPerdidos:
                                    prev[equipoActual] !== undefined &&
                                    resumenFecha[equipoActual].tipoResultado === "D"
                                        ? !!resumenFecha[equipoActual].jugado ? prev.tabla[equipoActual].partidosPerdidos + 1 : prev.tabla[equipoActual].partidosPerdidos
                                        : !!resumenFecha[equipoActual].jugado && resumenFecha[equipoActual].tipoResultado === "D"
                                        ? 1
                                        : 0,
                                golesaFavor:
                                    prev[equipoActual] !== undefined
                                        ? !!resumenFecha[equipoActual].jugado ? prev.tabla[equipoActual].golesaFavor +
                                        resumenFecha[equipoActual].resultado.favor.length : prev.tabla[equipoActual].golesaFavor
                                        : !!resumenFecha[equipoActual].jugado ? resumenFecha[equipoActual].resultado.favor.length : 0,
                                golesEnContra:
                                    prev[equipoActual] !== undefined
                                        ? !!resumenFecha[equipoActual].jugado ? prev.tabla[equipoActual].golesEnContra -
                                        resumenFecha[equipoActual].resultado.contra.length : prev.tabla[equipoActual].golesEnContra
                                        :  !!resumenFecha[equipoActual].jugado ? resumenFecha[equipoActual].resultado.contra.length : 0
                            }
                        ];
                    },
                    []
                );


                const listas =
                    actual.length > prev.length
                        ? {larga: actual, corta: prev}
                        : {larga: prev, corta: actual};

                let difference = listas.corta.filter(cortaTeam => listas.larga.filter(largaTeam => largaTeam.teamName === cortaTeam.teamName).length === 0);
                if (difference.length > 0) {
                    listas.larga.push({
                        teamName: difference[0].teamName,
                        logo: difference[0].logo,
                        goles: [],
                        rojas: [],
                        amarillas: [],
                        golesEnContra: 0,
                        golesaFavor: 0,
                        partidosJugados: 0,
                        partidosGanados: 0,
                        partidosPerdidos: 0,
                        partidosEmpatados: 0,
                        puntos: 0,
                    });
                }
                const newtabla = listas.larga.map(item => {
                    let temp = listas.corta.filter(
                        team => team.teamName === item.teamName
                    );

                    if (temp.length === 1) {
                        return {
                            teamName: item.teamName,
                            logo: item.logo,
                            goles: [...item.goles, ...temp[0].goles],
                            rojas: [...item.rojas, ...temp[0].rojas],
                            amarillas: [...item.amarillas, ...temp[0].amarillas],
                            golesEnContra: item.golesEnContra + temp[0].golesEnContra,
                            golesaFavor: item.golesaFavor + temp[0].golesaFavor,
                            partidosJugados: item.partidosJugados + temp[0].partidosJugados,
                            partidosGanados: item.partidosGanados + temp[0].partidosGanados,
                            partidosPerdidos:
                                item.partidosPerdidos + temp[0].partidosPerdidos,
                            partidosEmpatados:
                                item.partidosEmpatados + temp[0].partidosEmpatados,
                            puntos: item.puntos + temp[0].puntos
                        };
                    } else {
                        return {
                            teamName: item.teamName,
                            logo: item.logo,
                            goles: [...item.goles],
                            rojas: [...item.rojas],
                            amarillas: [...item.amarillas],
                            golesEnContra: item.golesEnContra,
                            golesaFavor: item.golesaFavor,
                            partidosJugados: item.partidosJugados,
                            partidosGanados: item.partidosGanados,
                            partidosPerdidos: item.partidosPerdidos,
                            partidosEmpatados: item.partidosEmpatados,
                            puntos: item.puntos
                        };
                    }
                });
                return newtabla;
            }, [])
            .sort((equipo1, equipo2) =>
                equipo2.puntos - equipo1.puntos === 0
                    ? equipo2.golesaFavor -
                    equipo2.golesEnContra -
                    (equipo1.golesaFavor - equipo1.golesEnContra)
                    : equipo2.puntos - equipo1.puntos
            );
    };

    render() {
        return (
            <Container>
                <Query query={FEED_QUERY}>
                    {({loading, error, data, refetch, networkStatus}) => {
                        return (
                            <Content
                                contentContainerStyle={{
                                    flex: error ? 1 : null
                                }}
                                refreshControl={
                                    <RefreshControl
                                        refreshing={loading}
                                        onRefresh={refetch}
                                        tintColor="#3478f6"
                                    />
                                }
                            >
                                {loading ?
                                    null :
                                    error ?
                                        <View
                                            style={{
                                                flex: 1,
                                                justifyContent: "center",
                                                alignItems: "center"
                                            }}
                                        >
                                            <Text> Error! Deslizá hacia abajo para volver a intentar </Text>
                                        </View> :
                                        (
                                            <Tabs
                                                bounces={false}
                                                contentProps={{indicatorStyle: "white"}}
                                                initialPage={0}
                                            >
                                                {data.categories.map((category, key) => {
                                                    return (
                                                        <CategoryTab
                                                            key={key}
                                                            meta={{
                                                                nombre: category.name,
                                                                anho: category.tournaments[0].year
                                                            }}
                                                            heading={category.name}
                                                            retrieving={loading}
                                                            listContent={this.listContent(
                                                                this.tableGenerator(category)
                                                            )}
                                                        />
                                                    );
                                                })}
                                            </Tabs>
                                        )}
                            </Content>
                        );
                    }}
                </Query>
            </Container>
        );
    }
}

const CategoryTab = props => (
    <Tab>
        <TabContent
            meta={props.meta}
            headerElements={["PJ", "PG", "PE", "PP", "GF","GC","+/-", "PTS"]}
        >
            {!props.retrieving ? props.listContent : <Spinner/>}
        </TabContent>
    </Tab>
);

const TabContent = props => (
    <List>
        <ListItem itemHeader>
            <Text style={{flex: 0.4, fontSize: 14}}>
                {`${props.meta.nombre.toUpperCase()} ${props.meta.anho}`}
            </Text>
            {props.headerElements.map((item, key) => (
                <Text key={key} style={styles.listContentDefaultItem}>
                    {item}
                </Text>
            ))}
        </ListItem>
        {props.children}
    </List>
);

const styles = StyleSheet.create({
    listContentDefaultItem: {
        flex: 0.075,
        fontSize: 14,
        textAlign: "center"
    }
});
