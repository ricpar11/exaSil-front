import React, { Component } from "react";
import {Container, Content, Icon, View} from "native-base";
import { Text, StyleSheet, Linking } from "react-native";

class Politica extends Component {
    render() {
        return (
            <Container>
                <Content contentContainerStyle={{ alignItems: "center" }}>
                    <View
                        style={{
                            width: "90%"
                        }}
                    >
                        <Text
                            style={{
                                ...styles.text,
                                fontSize: 28,
                                paddingBottom: "4%",
                                paddingTop: "4%"
                            }}
                        >
                            Privacy Policy
                        </Text>
                        <Text style={styles.pText}>
                            Ricardo Parra built the Exa Sil app as a Free app. This SERVICE
                            is provided by Ricardo Parra at no cost and is intended for use as
                            is.
                        </Text>
                        <Text style={styles.pText}>
                            This page is used to inform visitors regarding our policies with
                            the collection, use, and disclosure of Personal Information if
                            anyone decided to use our Service.
                        </Text>
                        <Text style={styles.pText}>
                            The terms used in this Privacy Policy have the same meanings as in
                            our Terms and Conditions, which is accessible at Exa Sil unless
                            otherwise defined in this Privacy Policy.
                        </Text>
                        <Text
                            style={{
                                ...styles.pText,


                                paddingTop: "2%"
                            }}
                        >
                            Information Collection and Use
                        </Text>
                        <Text style={styles.pText}>
                            For a better experience, while using our Service, we may require
                            you to provide us with certain personally identifiable
                            information, including but not limited to name, last name, email,
                            phone number. The information that we request will be retained by
                            us and used as described in this privacy policy.
                        </Text>
                        <Text style={styles.pText}>
                            The app does use third party services that may collect information
                            used to identify you.
                        </Text>
                        <Text style={styles.pText}>
                            Link to privacy policy of third party service providers used by
                            the app
                        </Text>
                        <Text
                            style={{ ...styles.pText, color: "blue" }}
                            onPress={() =>
                                Linking.openURL("https://www.facebook.com/policies/ads/")
                            }
                        >
                            • Facebook
                        </Text>
                        <Text
                            style={{ ...styles.pText, color: "blue" }}
                            onPress={() =>
                                Linking.openURL("https://segment.com/docs/legal/privacy/")
                            }
                        >
                            • Segment
                        </Text>
                        <Text
                            style={{
                                ...styles.pText,

                                paddingTop: "2%"
                            }}
                        >
                            Log Data
                        </Text>
                        <Text style={styles.pText}>
                            We want to inform you that whenever you use our Service, in a case
                            of an error in the app we collect data and information (through
                            third party products) on your phone called Log Data. This Log Data
                            may include information such as your device Internet Protocol
                            (“IP”) address, device name, operating system version, the
                            configuration of the app when utilizing our Service, the time and
                            date of your use of the Service, and other statistics.
                        </Text>
                        <Text
                            style={{
                                ...styles.pText,

                                paddingTop: "2%"
                            }}
                        >
                            Cookies
                        </Text>
                        <Text style={styles.pText}>
                            Cookies are files with a small amount of data that are commonly
                            used as anonymous unique identifiers. These are sent to your
                            browser from the websites that you visit and are stored on your
                            device's internal memory. This Service does not use these
                            “cookies” explicitly. However, the app may use third party code
                            and libraries that use “cookies” to collect information and
                            improve their services. You have the option to either accept or
                            refuse these cookies and know when a cookie is being sent to your
                            device. If you choose to refuse our cookies, you may not be able
                            to use some portions of this Service.
                        </Text>

                        <Text
                            style={{
                                ...styles.pText,

                                paddingTop: "2%"
                            }}
                        >
                            Service Providers
                        </Text>
                        <Text style={styles.pText}>
                            We may employ third-party companies and individuals due to the
                            following reasons:
                        </Text>

                        <Text style={styles.pText}>• To facilitate our Service;</Text>
                        <Text style={styles.pText}>
                            • To provide the Service on our behalf;
                        </Text>
                        <Text style={styles.pText}>
                            • To perform Service-related services; or
                        </Text>
                        <Text style={styles.pText}>
                            • To assist us in analyzing how our Service is used.
                        </Text>

                        <Text style={{ ...styles.pText, paddingTop: "2%" }}>
                            We want to inform users of this Service that these third parties
                            have access to your Personal Information. The reason is to perform
                            the tasks assigned to them on our behalf. However, they are
                            obligated not to disclose or use the information for any other
                            purpose.
                        </Text>
                        <Text
                            style={{
                                ...styles.pText,

                                paddingTop: "2%"
                            }}
                        >
                            Security
                        </Text>
                        <Text style={styles.pText}>
                            Cookies are files with a small amount of data that are commonly
                            used as anonymous unique identifiers. These are sent to your
                            browser from the websites that you visit and are stored on your
                            device's internal memory. This Service does not use these
                            “cookies” explicitly. However, the app may use third party code
                            and libraries that use “cookies” to collect information and
                            improve their services. You have the option to either accept or
                            refuse these cookies and know when a cookie is being sent to your
                            device. If you choose to refuse our cookies, you may not be able
                            to use some portions of this Service.
                        </Text>
                        <Text
                            style={{
                                ...styles.pText,

                                paddingTop: "2%"
                            }}
                        >
                            Links to Other Sites
                        </Text>
                        <Text style={styles.pText}>
                            This Service may contain links to other sites. If you click on a
                            third-party link, you will be directed to that site. Note that
                            these external sites are not operated by us. Therefore, we
                            strongly advise you to review the Privacy Policy of these
                            websites. We have no control over and assume no responsibility for
                            the content, privacy policies, or practices of any third-party
                            sites or services.
                        </Text>

                        <Text
                            style={{
                                ...styles.pText,

                                paddingTop: "2%"
                            }}
                        >
                            Changes to This Privacy Policy
                        </Text>
                        <Text style={styles.pText}>
                            We may update our Privacy Policy from time to time. Thus, you are
                            advised to review this page periodically for any changes. We will
                            notify you of any changes by posting the new Privacy Policy on
                            this page. These changes are effective immediately after they are
                            posted on this page.
                        </Text>
                        <Text
                            style={{
                                ...styles.pText,

                                paddingTop: "2%"
                            }}
                        >
                            Contact Us
                        </Text>
                        <Text style={{ ...styles.pText, paddingBottom: "4%" }}>
                            If you have any questions or suggestions about our Privacy Policy,
                            do not hesitate to contact us.
                        </Text>
                    </View>
                </Content>
            </Container>
        );
    }
}

export default Politica;

const styles = StyleSheet.create({
    text: {
    },
    pText: { fontSize: 16, paddingBottom: "2%" }
});
