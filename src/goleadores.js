import React, {Component} from "react";
import {RefreshControl, StyleSheet, View} from "react-native";
import {
    Container,
    Content,
    List,
    ListItem,
    Text,
    Icon,
    Tab,
    Tabs
} from "native-base";

import gql from "graphql-tag";
import {Query} from "react-apollo";

const FEED_QUERY = gql`
  query {
    categories {
      name
      id
      tournaments (where: {
      year: 2019,
      season: 2
      }){
        id
        year
        season
        events {
          id
          date
          matches {
            time
            home_team {
              name
              logo
            }
            away_team {
              name
              logo
            }
            highlights {
              player {
                name
              }
              team {
                name
              }
              action {
                description
              }
            }
          }
        }
      }
    }
  }
`;

export default class Goleadores extends Component {
    static navigationOptions = {
        tabBarIcon: () => <Icon name="plane"/>,
        title: "Goleadores"
    };

    dataGenerator = category => {
        let goleadores = category.tournaments[0].events.reduce(
            (prev, current, index, events) => {
                let eventGoals = events[index].matches.reduce(
                    (prev, current, index, matches) => {
                        const matchGoals = matches[index].highlights.reduce(
                            (prev, current, index, highlights) => {
                                let matchGoalsNew = {...prev};

                                if (highlights[index].action.description === "G") {
                                    if (
                                        matchGoalsNew[highlights[index].player.name] !== undefined
                                    ) {
                                        matchGoalsNew[highlights[index].player.name].goles =
                                            matchGoalsNew[highlights[index].player.name].goles + 1;
                                    } else {
                                        matchGoalsNew[highlights[index].player.name] = {
                                            goles: 1,
                                            equipo: highlights[index].team.name
                                        };
                                    }
                                }

                                return matchGoalsNew;
                            },
                            {}
                        );

                        let eventGoalsNew = {...prev};
                        Object.keys(matchGoals).forEach(player => {
                            if (eventGoalsNew[player] !== undefined) {
                                eventGoalsNew[player].goles =
                                    eventGoalsNew[player].goles + matchGoals[player].goles;
                            } else {
                                eventGoalsNew[player] = {
                                    goles: matchGoals[player].goles,
                                    equipo: matchGoals[player].equipo
                                };
                            }
                        });
                        return eventGoalsNew;
                    },
                    {}
                );
                let tournamentGoalsNew = {...prev};

                Object.keys(eventGoals).forEach(player => {
                    if (tournamentGoalsNew[player] !== undefined) {
                        tournamentGoalsNew[player].goles =
                            tournamentGoalsNew[player].goles + eventGoals[player].goles;
                    } else {
                        tournamentGoalsNew[player] = {
                            goles: eventGoals[player].goles,
                            equipo: eventGoals[player].equipo
                        };
                    }
                });
                return tournamentGoalsNew;
            },
            {}
        );
        return Object.keys(goleadores)
            .map(jugador => ({
                jugador,
                goles: goleadores[jugador].goles,
                equipo: goleadores[jugador].equipo
            }))
            .sort((jugador1, jugador2) => jugador2.goles - jugador1.goles);
    };

    render() {
        const listContent = data => {
            return data.map((item, key) => {
                const equipo = item.equipo.split(" ")[item.equipo.split(" ").length - 1]
                ;
                return (
                    <ListItem
                        key={key}
                        style={{
                            backgroundColor: `${key % 2 == 0 ? "#F9FCFE" : "#FFFFFF"}`,
                            width: "100%",
                            marginLeft: 0,
                            paddingLeft: 0
                        }}
                    >
                        <Text style={{flex: 0.9, marginLeft: "2%"}}>{item.jugador} ({equipo})</Text>
                        <Text style={{flex: 0.1, textAlign: "right"}}>{item.goles}</Text>
                    </ListItem>
                )
            });
        };

        return (
            <Container>
                <Query query={FEED_QUERY}>
                    {({loading, error, data, refetch, networkStatus}) => {
                        return (
                            <Content
                                contentContainerStyle={{
                                    flex: error ? 1 : null
                                }}
                                refreshControl={
                                    <RefreshControl
                                        refreshing={loading}
                                        onRefresh={refetch}
                                        tintColor="#3478f6"
                                    />
                                }
                            >
                                {loading ? null : error ?
                                    <View
                                        style={{
                                            flex: 1,
                                            justifyContent: "center",
                                            alignItems: "center"
                                        }}
                                    >
                                        <Text> Error! Deslizá hacia abajo para volver a intentar </Text>
                                    </View> : (
                                        <Tabs initialPage={0}>
                                            {data.categories.map((category, key) => (
                                                <Tab key={key} heading={category.name}>
                                                    <TabContent>
                                                        {listContent(this.dataGenerator(category))}
                                                    </TabContent>
                                                </Tab>
                                            ))}
                                        </Tabs>
                                    )}
                            </Content>
                        );
                    }}
                </Query>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    listHeader: {
        flexDirection: "row"
    }
});

const TabContent = props => <List>{props.children}</List>;
