import React, { Component, Fragment } from "react";
import { RefreshControl, StyleSheet } from "react-native";
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Thumbnail,
  Body,
  Text,
  Spinner,
  View
} from "native-base";
import axios from "axios";
import Icon from "react-native-vector-icons/MaterialCommunityIcons";

export default class DetallesEquipo extends Component {
  state = {
    detalles: null,
    retrieving: true,
    refreshing: false
  };

  render() {
    const { detalles } = this.props.navigation.state.params;
    return (
      <Container>
        <Content padder>
          <View>
            <Card>
              <CardItem bordered header>
                <Body style={styles.cardFirstItem}>
                  {detalles.logo && (
                    <View>
                      <Thumbnail source={{ uri: detalles.logo }} />
                    </View>
                  )}
                  <Text style={{ fontWeight: "bold", paddingTop: 5 }}>
                    {detalles.teamName}
                  </Text>
                </Body>
              </CardItem>
              {[
                ...new Set([
                  ...detalles.amarillas.map(amarilla => amarilla.playerName),
                  ...detalles.rojas.map(roja => roja.playerName),
                  ...detalles.goles.map(gol => gol.playerName)
                ])
              ]
                .sort(
                  (a, b) =>
                    detalles.goles.filter(gol => gol.playerName === b).length -
                    detalles.goles.filter(gol => gol.playerName === a).length
                )
                .map((jugador, key) => (
                  <CardItem
                    bordered
                    key={key}
                    style={{ flexDirection: "column" }}
                  >
                    <Text>{jugador}</Text>
                    <View
                      style={{
                        paddingTop: 10,
                        flexDirection: "row",
                        justifyContent: "center"
                      }}
                    >
                      {detalles.goles.filter(gol => gol.playerName === jugador)
                        .length > 0 && (
                        <View
                          style={{
                            flexDirection: "row",
                            alignItems: "center"
                          }}
                        >
                          <Icon name="soccer" size={25} />
                          <Text style={{ paddingLeft: 2 }}>
                            {
                              detalles.goles.filter(
                                gol => gol.playerName === jugador
                              ).length
                            }
                          </Text>
                        </View>
                      )}
                      {detalles.amarillas.filter(
                        amarilla => amarilla.playerName === jugador
                      ).length > 0 && (
                        <View
                          style={{
                            flexDirection: "row",
                            paddingLeft: 5,
                            alignItems: "center"
                          }}
                        >
                          <Icon color="#ffcc22" name="cards" size={25} />
                          <Text style={{ paddingLeft: 2 }}>
                            {
                              detalles.amarillas.filter(
                                amarilla => amarilla.playerName === jugador
                              ).length
                            }
                          </Text>
                        </View>
                      )}
                      {detalles.rojas.filter(
                        roja => roja.playerName === jugador
                      ).length > 0 && (
                        <View
                          style={{
                            flexDirection: "row",
                            paddingLeft: 5,
                            alignItems: "center"
                          }}
                        >
                          <Icon color="#ff3b30" name="cards" size={25} />
                          <Text style={{ paddingLeft: 2 }}>
                            {
                              detalles.rojas.filter(
                                roja => roja.playerName === jugador
                              ).length
                            }
                          </Text>
                        </View>
                      )}
                    </View>
                  </CardItem>
                ))}
            </Card>
          </View>
        </Content>
      </Container>
    );
  }
}

const styles = StyleSheet.create({
  cardFirstItem: {
    flex: 100,
    flexDirection: "column",
    justifyContent: "space-around",
    alignItems: "center"
  }
});
