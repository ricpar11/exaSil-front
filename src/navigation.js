import React from "react";
import {
    createBottomTabNavigator,
    createStackNavigator, createAppContainer
} from "react-navigation";
import {Platform, Easing, Animated} from "react-native";
import Tabla from "./tabla";
import Goleadores from "./goleadores";
import Fixture from "./fixture";
import DetallesPartido from "./detallesPartido";
import DetallesEquipo from "./detallesEquipo";
import Politica from "./politica";

import Icon from "react-native-vector-icons/MaterialCommunityIcons";

class TablaScreen extends React.Component {
    render() {
        return <Tabla {...this.props} />;
    }
}

class DetallesEquipoScreen extends React.Component {
    render() {
        return <DetallesEquipo {...this.props} />;
    }
}

class GoleadoresScreen extends React.Component {
    render() {
        return <Goleadores {...this.props} />;
    }
}

class FixtureScreen extends React.Component {
    render() {
        return <Fixture {...this.props} />;
    }
}
class DetallesPartidoScreen extends React.Component {
    render() {
        return <DetallesPartido {...this.props} />;
    }
}

const TablaStack = createStackNavigator(
    {
        Tabla: {screen: props => <TablaScreen {...props} />},
        DetallesEquipo: {screen: props => <DetallesEquipoScreen {...props} />}
    },
    {
        transitionConfig:
            Platform.OS === "android"
                ? () => ({
                    transitionSpec: {
                        duration: 300,
                        easing: Easing.out(Easing.poly(4)),
                        timing: Animated.timing
                    },
                    screenInterpolator: sceneProps => {
                        const {layout, position, scene} = sceneProps;
                        const {index} = scene;

                        const height = layout.initHeight;
                        const translateX = position.interpolate({
                            inputRange: [index - 1, index, index + 1],
                            outputRange: [layout.initWidth, 0, 0]
                        });

                        const opacity = position.interpolate({
                            inputRange: [index - 1, index - 0.99, index],
                            outputRange: [0, 1, 1]
                        });

                        return {opacity, transform: [{translateX}]};
                    }
                })
                : null,
        defaultNavigationOptions: ({navigation}) => ({
            gesturesEnabled: true,
            title: "Tabla"
        })
    }
);

const FixtureStack = createStackNavigator(
    {
        Fixture: {screen: props => <FixtureScreen {...props} />},
        DetallesPartido: {screen: props => <DetallesPartidoScreen {...props} />}
    },
    {
        transitionConfig:
            Platform.OS === "android"
                ? () => ({
                    transitionSpec: {
                        duration: 300,
                        easing: Easing.out(Easing.poly(4)),
                        timing: Animated.timing
                    },
                    screenInterpolator: sceneProps => {
                        const {layout, position, scene} = sceneProps;
                        const {index} = scene;

                        const height = layout.initHeight;
                        const translateX = position.interpolate({
                            inputRange: [index - 1, index, index + 1],
                            outputRange: [layout.initWidth, 0, 0]
                        });

                        const opacity = position.interpolate({
                            inputRange: [index - 1, index - 0.99, index],
                            outputRange: [0, 1, 1]
                        });

                        return {opacity, transform: [{translateX}]};
                    }
                })
                : null,
        defaultNavigationOptions: ({navigation}) => ({
            gesturesEnabled: true,
            title: "Fixture"
        })
    }
);

const GoleadoresStack = createStackNavigator(
    {
        Goleadores: {screen: props => <GoleadoresScreen {...props} />}
    },
    {
        transitionConfig:
            Platform.OS === "android"
                ? () => ({
                    transitionSpec: {
                        duration: 300,
                        easing: Easing.out(Easing.poly(4)),
                        timing: Animated.timing
                    },
                    screenInterpolator: sceneProps => {
                        const {layout, position, scene} = sceneProps;
                        const {index} = scene;

                        const height = layout.initHeight;
                        const translateX = position.interpolate({
                            inputRange: [index - 1, index, index + 1],
                            outputRange: [layout.initWidth, 0, 0]
                        });

                        const opacity = position.interpolate({
                            inputRange: [index - 1, index - 0.99, index],
                            outputRange: [0, 1, 1]
                        });

                        return {opacity, transform: [{translateX}]};
                    }
                })
                : null,
        defaultNavigationOptions: ({navigation}) => ({
            gesturesEnabled: true,
            title: "Goleadores"
        })
    }
);

const PoliticaStack = createStackNavigator(
    {
        Privaciidad: Politica
    },
    {
        transitionConfig:
            Platform.OS === "android"
                ? () => ({
                    transitionSpec: {
                        duration: 300,
                        easing: Easing.out(Easing.poly(4)),
                        timing: Animated.timing
                    },
                    screenInterpolator: sceneProps => {
                        const {layout, position, scene} = sceneProps;
                        const {index} = scene;

                        const height = layout.initHeight;
                        const translateX = position.interpolate({
                            inputRange: [index - 1, index, index + 1],
                            outputRange: [layout.initWidth, 0, 0]
                        });

                        const opacity = position.interpolate({
                            inputRange: [index - 1, index - 0.99, index],
                            outputRange: [0, 1, 1]
                        });

                        return {opacity, transform: [{translateX}]};
                    }
                })
                : null,
        defaultNavigationOptions: ({navigation}) => ({
            gesturesEnabled: true,
            title: "Política"
        })
    }
);

const Navigation = createBottomTabNavigator(
    {
        Tabla: TablaStack,
        Fixture: FixtureStack,
        Goleadores: GoleadoresStack,
        Privacidad: PoliticaStack

    },
    {
        defaultNavigationOptions: ({navigation}) => ({
            tabBarIcon: ({focused, tintColor}) => {
                const {routeName} = navigation.state;
                let iconName;
                if (routeName === "Tabla") {
                    iconName = "trophy";
                } else if (routeName === "Fixture") {
                    iconName = "soccer-field";
                } else if (routeName === "Goleadores") {
                    iconName = "soccer";
                } else if (routeName === "Privacidad") {
                    iconName = "eye-outline";
                }

                return <Icon name={iconName} size={25} color={tintColor}/>;
            }
        })
    }
);

const App = createAppContainer(Navigation);


export default App;
