import React from "react";
import Navigation from "./src/navigation";
import { StatusBar } from "react-native";
import { ApolloProvider } from "react-apollo";
import { InMemoryCache } from "apollo-cache-inmemory";
import ApolloClient from "apollo-boost";

const client = new ApolloClient({
  uri: "https://exa-sil-cms.herokuapp.com/graphql",
  cache: new InMemoryCache()
});

export default class Index extends React.Component {
  state = {
    torneos: [],
    tituloTorneo: "",
    isReady: false
  };

  render() {
    return (
      <ApolloProvider client={client}>
        <StatusBar barStyle="dark-content" />
        <Navigation screenProps={{ torneos: this.state.torneos }} />
      </ApolloProvider>
    );
  }
}
